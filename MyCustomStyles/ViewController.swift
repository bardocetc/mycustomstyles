//
//  ViewController.swift
//  MyCustomStyles
//
//  Created by gobierno de hidalgo on 03/19/2021.
//  Copyright (c) 2021 gobierno de hidalgo. All rights reserved.
//

import UIKit
import MyCustomStyles

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        view.backgroundColor = UIColor.primaryBlue
        DispatchQueue.main.async {
            [weak self] in
            guard let self = self else {return}
            let button = CustomButton()
            button.setTitle("mi boton favorito", for: .normal)
            button.setStyle(.blueRounded)
            
            self.view.addSubview(button)
            
            //NSLayoutConstraint.activate(<#T##constraints: [NSLayoutConstraint]##[NSLayoutConstraint]#>)
        }
        
        
        
        
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

